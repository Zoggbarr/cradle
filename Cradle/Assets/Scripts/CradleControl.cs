﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CradleControl : MonoBehaviour {

    private List<CradlePend> pendulums;
    public int numBalls;
    public float stringLength;
    public CradlePend pend_prefab = null;
    public float gravity;
    public float airRes;
    private bool paused = false;

    // Use this for initialization
	void Start () {
        pendulums = new List<CradlePend>();
        pendulums.AddRange(SpawnPendulum());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (paused)
            {
                paused = false;
            }
            else
            {
                paused = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            foreach (CradlePend pend in pendulums)
            {
                pend.ResetState();
            }
        }
    }

    void FixedUpdate ()
    {   
        if (false == paused) { 
            foreach (CradlePend pend in pendulums)
            {
                pend.UpdateState();
            }
        }
        bool collision = true;
        while (collision) {
            collision = false;
            for (int i = 0; i < numBalls; i++)
            {
                CradlePend pend = pendulums[i];
                if (i > 0 && pend.Angle < pendulums[i-1].Angle)
                {
                    pend.swapState(pendulums[i - 1]);
                    collision = true;
                }
                if (i < numBalls - 1 && pend.Angle > pendulums[i + 1].Angle)
                {
                    pend.swapState(pendulums[i + 1]);
                    collision = true;
                }
            }
        }
        foreach (CradlePend pend in pendulums)
        {
            pend.UpdatePosition();
        }
    }

    public IEnumerable<CradlePend> SpawnPendulum()
    {
        for (int i = 0; i < numBalls; i++)
        {
            float offset = i-numBalls/2.0f;
            Vector3 startPos = new Vector3(offset,0,0) 
                             + transform.position;
            CradlePend pendulum = Instantiate(pend_prefab, 
                startPos, new Quaternion()) as CradlePend;
            pendulum.cradle = this;
            pendulum.pivot = startPos;
            yield return pendulum;
        }
    }
}
