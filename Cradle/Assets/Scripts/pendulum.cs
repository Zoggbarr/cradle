﻿using UnityEngine;
using System.Collections;

using System;

public class pendulum : MonoBehaviour {

    private Vector3 position;
    public Vector3 pivot;
    private float angle;       // position in radians
    private float velocity;    // Velocity rad/s
    public float length;
    public float startAng;
    public float startVel;
    public float gravity;
    public float airRes;
    public float dt;
    public float radius;

    // Use this for initialization
    void Start ()
    {
        angle = startAng;
        velocity = startVel;
        position = transform.position;
        transform.localScale = new Vector3(1, 1, 1) * radius;
        UpdatePosition();
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    void FixedUpdate ()
    {
        float force = Force();
        angle += velocity * dt + 0.5f * force * dt * dt;
        float force2 = Force();
        velocity += 0.5f * (force + force2) * dt;
        //velocity += force * dt;
        //angle += velocity * dt;
        UpdatePosition();
    }

    void UpdatePosition()
    {
        position.x = length * Mathf.Sin(angle);
        position.y = -length * Mathf.Cos(angle);
        transform.position = position + pivot;
    }

    float Force()
    {
        return -gravity/length * Mathf.Sin(angle) 
               - airRes * velocity*Math.Abs(velocity);
    }
}
