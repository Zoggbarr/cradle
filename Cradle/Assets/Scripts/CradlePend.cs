﻿using UnityEngine;
using System.Collections;

using System;

public class CradlePend : MonoBehaviour {

    private Vector3 position;
    private float angle;       // position in radians
    private bool draged = false;
    private Vector2 dragOrigin;

    public float Angle{ get { return angle; } }
    float velocity;    // Velocity rad/s
    public CradleControl cradle;
    public Vector3 pivot;
    public float startAng;
    public float startVel;
    public float radius = 1.0f;

    // Use this for initialization
    void Start ()
    {
        angle = startAng;
        velocity = startVel;
        position = transform.position;
        transform.localScale = new Vector3(1, 1, 1) * radius;
        UpdatePosition();
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    void FixedUpdate ()
    {

    }

    public void OnMouseDrag()
    {
        if (draged == false)
        {
            draged = true;
            dragOrigin = Input.mousePosition;
        }
        //velocity = 0;
        Vector3 mouse = Input.mousePosition;
        Vector3 screenPivot = Camera.main.WorldToScreenPoint(pivot);
        float x = mouse.x - screenPivot.x;
        float y = mouse.y - screenPivot.y;
        float newAngle = Mathf.Atan2(y, x) + Mathf.PI * 0.5f;
        velocity = (newAngle - angle) / Time.deltaTime * 0.1f; //* Mathf.Rad2Deg * 10;
        angle = newAngle;
    }

    public void OnMouseUp()
    {
        draged = false;
    }

    public void UpdateState()
    {
        if (draged == false)
        {
            float force = Force();
            angle += velocity * Time.deltaTime + 0.5f * force * Time.deltaTime * Time.deltaTime;
            float force2 = Force();
            velocity += 0.5f * (force + force2) * Time.deltaTime;
            //angle = angle % Mathf.PI;
        }
    }

    public void UpdatePosition()
    {
        position.x = cradle.stringLength * Mathf.Sin(angle);
        position.y = -cradle.stringLength * Mathf.Cos(angle);
        transform.position = position + pivot;
    }

    public float Force()
    {
        return -cradle.gravity / cradle.stringLength * Mathf.Sin(angle) 
               - Math.Abs(cradle.airRes) * velocity*Math.Abs(velocity);
    }

    public void swapState(CradlePend other)
    {
        float tempAngle = angle;
        float tempvelocity = velocity;
        if (false == draged)
        {
            velocity = other.velocity;
            angle = other.angle;
        }
        if (false == other.draged)
        { 
            other.angle = tempAngle;
            other.velocity = tempvelocity;
        }
    }
    
    public void ResetState()
    {
        angle = startAng;
        velocity = startVel;
        UpdatePosition();
    }
}
